"use strict"


// Our services tabs
const tabs = document.querySelectorAll('.our-services .tabs li');
const contents = document.querySelectorAll('.our-services .tabs-content li');

tabs.forEach((tab, tabIndex) => {
    tab.onclick = () => {
        tabs.forEach((item) => {
            item.classList.remove('active');
        })
        tab.classList.add('active');

        contents.forEach((paragraph, paragraphIndex) => {
            if (paragraphIndex === tabIndex) {
                paragraph.style.display = 'flex';
            } else {
                paragraph.style.display = 'none';
            }
        })
    }
})


// our amazing work tabs
const ourWorkTabs = document.querySelectorAll('.our-amazing-work .tabs-titles li');
const sections = document.querySelectorAll('.our-amazing-work .load-more-tabs li');

ourWorkTabs.forEach((tab) => {
  tab.onclick = () => {
    ourWorkTabs.forEach((item) => {
      item.classList.remove('active');
    })
    tab.classList.add('active');

    const selectedSectionName = tab.id;
    if (selectedSectionName === 'all') {
      sections.forEach((section) => {
        section.style.display = 'flex';
      })
    } else {
      sections.forEach((section) => {
        if(section.classList.contains(selectedSectionName)) {
          section.style.display = 'flex';
        } else {
          section.style.display = 'none';
        }
      })
    }
  }
})

const photos = document.querySelector('.load-more');
const buttonPhoto = document.querySelector('.load-more-12photos');

 buttonPhoto.onclick = () => {
   photos.classList.remove('hidden');
   buttonPhoto.classList.add('hidden');
}

// slider
new Swiper('.swiper', {
  effect: 'Coverflow',
  speed: 500,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  slidesPerView: 1,
  initialSlide: 1,
  centeredSlides: true,
  loop: true,
  grabCursor: true,

  thumbs: {
    swiper: {
      el: '.swiper-mini',
      slidesPerView: 4,
      initialSlide: 1,
      slidesPerGroup: 2,
      loop: true,
    },
  },
});
